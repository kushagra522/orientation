##GIT
**What is git ?**
Git is a version-control software that keeps track of every revision you and your team makes during the development of a software.

##Required Vocabulary

**Repository:**
It is a collection of all code files and folders.It's the big box where team members through their codes.


**GitLab:**
The second most popular remote storage solution for git repos.


**Commit:**
It is same as saving your work.It only exists on your local machine until it is pushed to a remote repository.


**Push:**
It is essentially syncing your commits to gitlab.


**Branch:**
Git repos are similar to a tree.
Master Branch


**Merge:**
Integrating two branches together.


**Clone:**
It takes the entire online repository and creates an exact copy of it in your machine.

**Fork:**

Same as cloning, the only difference is that here you get an entirely new repo of that code in your own name.
Getting started and required command
How to install git
For Linux:
      sudo apt-get install git 


For Windows:
download the installer and run it.



**Git Internals**
Modified
Staging
Commit
