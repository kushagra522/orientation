Docker Terminologies

1.Docker

Program for developers to develop and run applications with containers.

2.Docker Images

contains code, runtime, libraries, environment variables and configuration files.

3.Container

Running docker images
from one image we can create multiple containers.

4.Docker Hub

It is like a github for docker images and containers.


Docker Installation

1.Installing CE (Community Docker Engine)
  $ sudo apt-get update
  $ sudo apt-get install \
      apt-transport-https \
      ca-certificates \
      curl \
      gnupg-agent \
      software-properties-common
  $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  $ sudo apt-key fingerprint 0EBFCD88
  $ sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
      stable nightly test"
  $ sudo apt-get update
  $ sudo apt-get install docker-ce docker-ce-cli containerd.io

   // Check if docker is successfully installed in your system
  $ sudo docker run hello-world

Docker Basic Commands

$ docker ps  allows us to view all the containers that are running on the Docker Host.<br>
$ docker start  starts any stopped container(s).<br>
$ docker stop  stops any running container(s).<br>
$ docker run  it creates containers from docker images.<br>
$ docker rm    it deletes the containers.<br>
$ docker cp   copy file inside docker.<br>
$ docker exec   give permission to run shell script.<br>
$ docker tag  tag image name with different name.<br>
$ docker push   push on dockerhub.